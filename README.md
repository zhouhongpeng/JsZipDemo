# JSZip cocos creator 使用示例
#注 ：cocos creato版本 : 2.3.0

## jszip
jszip是一个用于创建、读取和编辑.zip文件的JavaScript库
## creator引用 jszip讲解
1. **jszip库位置，将其设置为插件**
   Script > lib > jszip.js(jszip.d.ts)

![设置截图](https://upload-images.jianshu.io/upload_images/8742246-beca121dbcb452a4.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
2. **创建压缩文件**
将需要压缩的文件或者文件夹选中，使用压缩工具将其压缩为.zip 文件，
注：因为编码格式的原因，压缩成zip文件后需要将后缀.zip手动改为.bin
![目录截图](https://upload-images.jianshu.io/upload_images/8742246-f1597b24275f97aa.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

3. **代码解读**
![代码截图](https://upload-images.jianshu.io/upload_images/8742246-601c0be88fffc412.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)