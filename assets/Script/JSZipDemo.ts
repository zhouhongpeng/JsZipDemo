const {ccclass, property} = cc._decorator;

@ccclass
export default class NewClass extends cc.Component {

    @property(cc.Label)
    labID: cc.Label = null;

    @property(cc.Label)
    labInfoAll: cc.Label = null;

    @property
    text: string = 'hello';

    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        this.initJson();
    }
    //进行压缩文件的读取以及解压
    initJson(){
        let strZipUrl = cc.url.raw("resources/json/enemy.bin");
        cc.loader.load({url :strZipUrl , type : "binary"} , (err, res) => {
			if(!err){
                JSZip.loadAsync(res).then((zip : JSZip)=>{
                    return zip.file("enemy.json").async('text');
                }).then((data:string)=>{
                    let jsonData = JSON.parse(data);
                    this.labInfoAll.string = data;
                    this.labID.string = jsonData[0].id;
                });
            }
		});
    }
    // update (dt) {}
}
